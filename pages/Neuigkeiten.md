---
layout: page
title: Neuigkeiten
permalink: /neuigkeiten/
nav_order: 3
---

**Update - Aktionswechsel** - 28.01.2022

Wir haben uns dazu entschieden das Baumhaus abzubauen und die Aktionsform des Klimaprotests in Bayreuth zu wechseln. Für uns ist klar **"Das Baumhaus geht, der Protest geht weiter!"**. Wir wollen die enstandene Vernetzung der Klimagerechtigkeitsbewegung in Bayreuth nutzen und viele kreative Aktionen zu unseren Forderungen machen.

Es war ein schwieriger Entscheidungsprozess und letztendlich haben uns einige verschiedene Gründe zum Wechsel der Aktionsform bewegt. Zunächst einmal haben wir das Ziel die Klimagerechtigkeitsbewegung in Bayreuth zu vernetzen und zu stärken erreicht. Viele Menschen haben sich in den letzten Wochen und Monaten kennengelernt und es sind noch mehr Ideen entstanden. Leider konnten wir diese bislang noch nicht umsetzen, weil der Erhalt des Baumhaus nötige Kapazitäten gebunden hat. Außerdem haben sich die Bayreuther*innen und die Stadt mitlerweile an uns gewöhnt und wir glauben mit anderen Aktionen mehr Druck auf den Stadtrat ausüben zu können endlich effektiven Klimaschutz zu betreiben. Und nicht zuletzt hat auch die jüngste Corona-Welle zur Entscheidung beigetragen. Bei Inzidenzen von aktuell über 1000 in Bayreuth halten wir es für unvernünftig weiterhin den engen Kontakt untereinander und zu den vorbeigehenden Menschen zu halten. Gerade bei Übernachtungen auf dem Baumhaus ist ein Einhalten der Mindestabstände nicht umsetzbar.

![pic9](/assets/pic9.jpg){: width="350" }
![pic10](/assets/pic10.JPG){: width="350" }

Wir bleiben als Gruppe Klimagerechtigkeitsaktivist*innen aber weiter aktiv in Bayreuth. Wir planen wöchentliche Mahnwachen immer Freitags und vielfältige Aktionen zu unseren Forderungen an die Stadt. Außerdem haben wir dem Stadtrat zwei konkrete Forderungen mitgegeben, welche wir in 5 Monaten für umsetzbar halten.

1) Ausrufung des Klimanotstandes

2) Einführung eines Beratungsangebotes für Mieter*innenstromprojekte

Wir werden die Entwicklungen im Bayreuther Stadtrat genau verfolgen und mit dem Baumhaus wiederkommmen, sollte die Stadt immer noch nicht ernsthaft auf den Weg zur Klimagerechtigkeit begeben.

Wir freuen uns auch weiterhin immer über neue Gesichter. Wenn ihr Lust habt mit uns zusammen die Stadt Bayreuth zu mehr Klimagerechtigkeit aufzufordern, meldet euch gerne per Mail oder über Social Media bei uns!

![pic11](/assets/pic11.JPG){: width="350" }
![pic12](/assets/pic12.JPG){: width="350" }



**Winterupdate** - 21.12.2021

Die Weihnachtsfeiertage kommen näher, und ebenso wie die Stadtverwaltung Pause macht, möchten auch von uns viele mit ihrer Familie und ihren Freunden feiern. Deshalb machen wir vom 24ten bis zum 26ten eine Pause. Dafür werden wir unsere Strukturen vorher Winterfest machen. Ab dem 27ten sind wir dann wieder jeden Tag, aber während der Winterferien nicht dauerhaft, da. Danach wieder täglich. Die Stadt möchte jedoch diesen Versammlungsort nur bis 31. Januar genehmigen.
Wenn du dich auch über die Weihnachtszeit einbringen möchtest, melde dich einfach bei uns. Wir freuen uns!

![pic7](/assets/pic7.jpeg){: width="350" }
![pic8](/assets/pic8.jpeg){: width="350" }

**Offenen Gespräch** - 03.12.2021

Das Treffen heute war ermutigend und ernüchternd. 
Es waren ca. 20 Menschen, davon viele aus dem Stadtrat. Danke für das Feedback und das offene Gespräch. 
Wäre das heute aber repräsentativ für Bayreuth, fragen wir uns, warum wir noch da sind, warum nichts passiert? 
Wo waren die Stadträt*innen, die bremsen, blockieren und die Klimakrise scheinbar marginalisieren? Wo war CSU und FDP? Warum ist die SPD gegangen? 

Thomas Ebersberger (CSU, OB) war leider verhindert, es gibt aber ein Gesprächsangebot sobald es wieder möglich ist. Andreas Zippel (SPD, 2. Bürgermeister) war nachmittags schon zu einem Gespräch da. 

Wir sind weiter jeden Tag für Klimagerechtigkeit und unsere Forderungen da. Bayreuth muss sich bewegen.

![pic5](/assets/pic6.jpeg)

**Das Klimabaumhaus lädt ein zum offenen Gespräch** - 29.11.2021
 
Am 02. Dezember um 18 Uhr lädt das Klimabaumhaus die Mitglieder des Stadtrats, den Oberbürgermeister zum öffentlichen Gespräch über lokale Lösungen für die Klimakrise und die Forderungen des Klimabaumhauses ein. 
 
Die Klimakrise ist die größte Krise unserer Zeit und ihre Bewältigung ist für die Menschen eine Überlebensfrage. Doch auch wenn es eine globale Krise ist brauchen wir lokale Lösungen dafür. Die Umsetzung unserer Forderungen ist dabei ein erster Schritt um Verantwortung für die Klimakrise zu übernehmen.

Kommt gerne alle dazu, es ist ein offenes Treffen! Bitte vorher einen Schnelltest machen und vor Ort Maske tragen


**Das Klimabaumhaus lebt noch!** - 26.11.2021

Neue Banner wurden aufgehängt. Eines davon, um Aufmerksamkeit für den Internationaler Tag zur Beseitigung von Gewalt gegen FLINTA* zu schaffen!  Die Klimakrise löst sich nicht auf, ohne einen gesellschaftlichen Wandel, der die sozial Ebene auch berücksichtig. Gewalt und Diskriminierung sind in allen Formen zu verurteilen. 

![pic5](/assets/pic5.jpg)

**Bayreuths Zeit läuft!** - 25.11.2021

![pic1](/assets/pic1.jpeg){: width="350" }
![pic2](/assets/pic2.jpeg){: width="350" }

**Es geht los** - 24.11.2021

![pic0](/assets/pic0.jpg)

Diese Nacht wurde in Bayreuth ein Protestbaumhaus vor dem Rathaus gebaut, die Klimagerechtigkeitsaktivist*innen die an dem Projekt beteiligt sind fordern von der Stadt, als einen ersten Schritt in Richtung Klimagerechtigkeit ihre 7 Forderungen zu erfüllen. 📝🌎🌍🌏

Wenn du Infos zu dem Projekt haben möchtest kannst du gerne diesem Infochannel beitreten: https://t.me/Klimabaumhausbayreuth oder uns auf Instagramm, Twitter oder Facebook folgen. Wir freuen uns auch immer sehr über interessierte Menschen die einfach mal vorbei kommen. 💡

Wenn du uns unterstützen möchtest schreib mir am besten eine Direktnachricht. 😊
