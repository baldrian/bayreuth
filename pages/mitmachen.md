---
layout: page
title: Unterstützen
permalink: /mitmachen/
nav_order: 4
---

Möchtest du gern uns unterstützen, aber du weisst nicht wie?

Am besten meldest du dich bei uns per Mail oder über Social Media. Dann können wir zusammen besprechen wie und wo du dich gut einbringen kannst.

Du hast keine Zeit aber Geld übrig? 
Da wir alle ehrenamtlich an diesem Projekt arbeiten und immer wieder auch Kosten anfallen, freuen wir uns auch über finanzielle Unterstützung.   

    Empfänger: Spenden&Aktionen
    IBAN: DE29 5139 0000 0092 8818 06
    BIC: VBMHDE5F
    Verwendungszweck: Klimabaumhaus Bayreuth


  
