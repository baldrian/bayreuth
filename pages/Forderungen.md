---
layout: page
title: Forderungen
permalink: /forderungen/
nav_order: 3
---

  Wir haben ein Baumhaus am Bayreuther Rathaus gebaut um auf die aktuelle Klimakatastrophe und den dringenden Handlungsbedarf durch die Stadt Bayreuth hinzuweisen. Unsere Forderungen stellen wir im Folgenden vor. Wir sind unter klimacamp_bayreuth@riseup.net und in Social Media erreichbar. Am liebsten aber natürlich persönlich und vor Ort.

 Klick auf die kleine Beschreibung um mehr Infos zu erhalten.

<br>

- Treibhausgasrestbudget einhalten
  <details><summary>
  Wir fordern von der Stadt Bayreuth ihr Treibhausgasrestbudet nicht zu überschreiten und damit einen Beitrag zum Einhalten der 1,5 Grad Grenze zu leisten.
  </summary>
  <br>

  Bei gleichbleibender Entwicklung der Treibhausgas-Emissionen hat die Stadt Bayreuth ihr Restbudget zur Einhaltung der 1,5 Grad Grenze laut eigenen Berechnungen bereits Mitte 2024 aufgebraucht. Selbst unter Berücksichtigung von Kompensationen und bei einer deutlich verstärkten Reduktion um 5,5% pro Jahr sei eine Verlängerung des Restbudgets nur bis maximal 2026 möglich. [1]
  <br>
  
  Unsere Berechnungen, basierend auf denselben Daten, sind sogar noch erschreckender. Das Restbudget ist demnach bei gleichbleibender Entwicklung der Treibhausgase bereits Mitte 2023 aufgebraucht. Schon jetzt verbleibt uns nur noch ca. 1 Mt CO2. Spätestens ab dann emittieren wir deutlich über die vereinbarte 1,5 Grad Grenze hinaus und leisten somit nicht den nötigen Beitrag zur Bekämpfung der Klimakrise. Selbst wenn wir so eines Tages Klimaneutralität erreichen, hätten wir deutlich über unser Maß hinaus zu dieser Katastrophe beigetragen.
  <br>
  
  Dahingehende Bestrebungen des Stadtrats, diese Ziele zu erreichen, sind bisher nicht ersichtlich. Somit verfehlt Bayreuth noch deutlich weiter als bisher angenommen die Ziele der Pariser Klimaabkommen. Das ist nicht hinnehmbar!
  <br>
  
  Zum Vergleich: In der gesamten Zeit von 2010 bis 2019 gab es in Bayreuth eine Abnahme der Treibhausgasemissionen pro Kopf um 19%, solch eine Abnahme brauchen wir eigentlich in den nächsten Jahren jedes Jahr, mindestens! Bayreuth muss sich also ranhalten!</details>
<br>

- MAPA Klimapartnerstadt unterstützen
  <details><summary>
  Die Schäden, die durch die Klimakatastrophe entstehen, treffen nicht alle Gebiete gleich. Durch eine Städtepartnerschaft kann Bayreuth nicht nur auf persönlicher Ebene mit den am stärksten betroffenen Menschen (MAPA) Beziehungen aufbauen, sondern auch Verantwortung an dieser Katastrophe übernehmen.
  </summary>
  <br>

  Klimagerechtigkeit muss global gedacht werden. Während die wohlhabenden Länder hauptverantwortlich für Treibhausgas-Emissionen und damit für die Klimakrise sind, müssen die Gebiete, die am wenigsten für die Klimakrise können, die Folgen am stärksten ausbaden. Wir alle tragen eine Verantwortung gegenüber diesen Menschen und Gebieten, auch MAPA (Most Affected People and Areas) genannt.
  <br>

  Klimagerechtigkeit bedeutet daher nicht nur die starke Reduzierung von Treibhausgas-Emissionen sondern auch diese Verantwortung, für die Schäden die durch die Klimakrise entstanden sind, zu übernehmen. Der persönliche Abstand macht es uns leicht, diese Probleme in unserem Alltag zu vergessen. Probleme, die für andere Menschen den Alltag dominieren.
  <br>

  Eine Städtepartnerschaft zwischen Bayreuth und einer MAPA-Stadt bietet die Möglichkeit, einen persönlichen, menschlichen Kontakt herzustellen. Bietet die Möglichkeit die Folgen unseres Handelns direkter wahrzunehmen und anzuerkennen. Und bietet die Möglichkeit auf Augenhöhe über Lösungen und Strategien nachzudenken und so, in einem echten Miteinander, in Solidarität, uns dieser Krise zu stellen. Ohne dabei natürlich die Verantwortung Bayreuths an dieser Krise zu vergessen und dann z.B. die Partnerstadt im (Wieder-)Aufbau von Infrastruktur zu unterstützen.
  <br>

  Bisherige Partnerstädte von Bayreuth kommen aus ähnlich privilegierten Gebieten, weshalb eine neue Partnerstadt gefunden werden muss:

  Annecy/Frankreich (seit 1966), La Spezia/Italien (seit 1999), Rudolstadt/Thüringen (seit 1990), Prag 6/Tschechien (seit 2008), Burgenland/Österreich (seit 1990), Tekirdag/Süleymanpasa/Türkei (seit 2012)</details>
<br>

- Energetische Sanierung aller Altbauten
  <details><summary>
  Durch die energetische Sanierung aller Altbauten könnte einerseits die Stadt Bayreuth langfristig einen beachtlichen Teil der Treibhausgasemissionen einsparen. Andererseits können so die Bayreuther:innen Heizkosten sparen. Die Sanierung muss sozialverträglich durchgeführt werden.
  </summary>
  <br>

  2019 hat Wärme ganze 58% des Endenergieverbrauchs in Bayreuth ausgemacht. Die anderen 42% setzen sich aus 22% Kraftstoffe und 20% Strom zusammen. [1] In der Energiewende muss deshalb der Punkt Wärme an zentraler Stelle mit bedacht werden, denn in diesem Bereich lässt sich noch jede Menge einsparen. Ein großer Schritt wäre die energetische Sanierung aller Altbauten und damit auch die Umrüstung weg von Gas- oder Ölheizungen.
  <br>

  Um im Bereich Wohnen bis 2035 eine wirkliche Treibhausgasneutralität zu erreichen, müssten bisherige Anstrengungen vervielfacht werden. Ein Fokus muss sein, durch Anreize, Förderungen und Beratung die Sanierungsrate und Intensität aller bestehenden Gebäude schnellstmöglich auf 5% / Jahr zu steigern. [2]
  <br>

  Die bisherige jährliche Sanierungsquote in Bayreuth liegt bei ca. 1%, was in etwa dem deutschen Durchschnitt entspricht. Damit verfehlt Bayreuth selbst das ungenügende Ziel der Metropolregion Nürnberg, welches bei 2% liegt. [3]
  <br>

  Dabei dürfen die Sanierungskosten nicht auf die Mieter:innen umgeschlagen werden. Die Sanierung muss sozial verträglich gestaltet werden, um weiterhin bezahlbaren Wohnraum zu gewährleisten. Die aktuelle Gaspreisentwicklung trifft vor allem diejenigen, die in schlecht sanierten Altbauten sitzen. Sanierung hilft hier auch der sozialen Gerechigkeit und so können Bayreuther:innen langfristig Heizkosten sparen.
  <br>

  Das ist nur ein erster, wenn auch zentraler Schritt in der notwenigen Wohnwende. Die Sanierung kann zudem helfen Leerstand in qualitativen Wohnraum zu verwandeln und so Neubauten zu verhindern. Denn gerade der Bausektor ist für sehr viele Klimaschäden verwantwortlich.</details>
<br>

- Mieter:innenstromprojekte & Bürger:innenenergie fördern
  <details><summary>
  Wir brauchen eine schnelle Energiewende, an der alle Menschen mit profitieren können. Deshalb sollten Mieter:innenstromprojekte und Bürger:innenenergie massiv gefördert werden.
  </summary>
  <br>

  Wir brauchen jetzt eine Energierevolution, doch das darf nicht bedeuten, dass nur wenige daran verdienen. Strom aus Sonne und Wind sind jetzt schon günstiger als Kohle und Atomstrom. Auf der Stromrechnung zeigt sich das jedoch nicht wieder, es sei denn, es ist die eigene Photovoltaik Anlage auf dem eigenen Dach. Dies schließt viele Menschen von den Vorteilen der Energiewende aus.
  <br>

  Nachhaltige Energie muss auch sozial nachhaltig sein! In Mieter:innenstromprojekten können Menschen, die in Mietswohnungen leben, direkt an den günstigen Gestehungskosten des erneuerbaren Stroms profitieren. Konkret können sie z.B. den Solarstrom, der auf dem Dach des Hauses, in dem sie selbst leben, erzeugt wird, zu günstigen Preisen bekommen. So können sich zudem mehr Menschen mit der Energiewende identifizieren. Wenn anderswo die Strompreise steigen, kann er beim Mieter:innenstrom sogar sinken. Solche Projekte werden vom Bund gefördert, die Anzahl umgesetzter Projekte ist jedoch bislang noch zu niedrig. [4] Daher fordern wir eine Förderung dieser Projekte durch Beratungs- und Informationsangebote und eine Unterstützung der Stadtwerke Bayreuth in solchen Projekten. Vor allem bei den bürokratischen Hürden sehen wir hier Handlungsbedarf.
  <br>

  Weiterhin gibt es die Möglichkeit selbst Teil der Energiewende zu werden in Form von Bürger:innenenergieprojekten. Uns ist bislang kein solches Projekt in Bayreuth bekannt, in dem genossenschaftlich organisiert auch größere Photovoltaik oder Windkraftwerke errichtet und betrieben werden. Eine transparente, genossenschafltiche Eigenbeteiligung kann auch die Akzeptanz solcher Kraftwerke deutlich erhöhen. Die Stadt Bayreuth sollte solche Projekte mit initiieren und fördern, in dem sie z.B. ihre Dachflächen für Bürger:innenenergie zur Verfügung stellen. So kann Bayreuth nicht nur über Zertifikate Ökostrom beziehen, sondern selbst einen Beitrag leisten.</details>
<br>

- Fahrradspur auf dem Ring
  <details><summary>
  Eine Fahrradspur auf dem Ring soll das Fahrradfahren in Bayreuth attraktiver und sicherer machen.
  </summary>
  <br>

  Wer in Bayreuth regelmäßig Fahrrad fährt kennt das Problem: Auf dem Ring gibt es weder einen Fahrradstreifen noch einen Fahrradweg, auf dem Fußweg fahren ist oft verboten, also sind die einzigen Möglichkeiten dort auf der Straße zu fahren, einen oft erheblicher Umweg in Kauf zu nehmen, oder die Fahrt durch die Innenstadt.  Die Straße kommt für viele nicht in Frage, denn zwischen den schnellen Autos und LKWs auf dem Ring fährt es sich nicht besonders sicher.
  <br>

  Dieser Umstand macht das Fahrradfahren in Bayreuth unattraktiv und begünstigt eher die Wahl zum Auto, dabei ist Bayreuth klein genug um die meisten Strecken mit dem Rad zu fahren und das ohne deutlich mehr Zeitaufwand. Mit einer guten Fahrradinfrastruktur kann dem Abhilfe geschaffen werden und somit sogar die Innenstadt noch ruhiger und fußgänger:innenfreundlich werden. Eine Fahrradspur auf dem Ring ist dabei nur ein erster Schritt für eine Verkehrswende. Die Vorschläge, die der Radentscheid entwickelt hat, enthalten weiter gute und umsetzbare Maßnahmen. [5]</details>
<br>

- Kostenloser ÖPNV überall und für alle
  <details><summary>
  Kostenloser und gut ausgebauter ÖPNV wäre ein riesen Anreiz für Menschen aufs Auto zu verzichten.
  </summary>
  <br>

  ÖPNV sollte eine gute Alternative zum Autofahren sein, dafür ist er in Bayreuth allerdings bisher nicht ausreichend ausgebaut: Bei Glatteis im Winter sind 40 Minuten zu Fuß zur Universität teilweise schneller als mit dem Bus zu fahren. Jede Menge Schüler:innen kommen morgens viel zu früh zur Schule, da die Züge und Busse so selten fahren. Abends können Menschen, die weiter außerhalb wohnen und kein Auto haben, sich in Bayreuth nur dann mit Leuten treffen, wenn sie sich zuvor einen Übernachtungsplatz organisiert haben oder sie von ihren Eltern abgeholt werden können.
  <br>

  Für viele Menschen ist der Preis für ein Ticket zudem viel zu teuer. Diese Menschen können so nicht am sozialen Leben in der Stadt teilhaben, wenn sie nicht die hohen Strafen für Schwarzfahren riskieren wollen. Grade ältere Menschen, die aus Sicherheitsaspekten oder finanziellen Gründen kein Auto mehr fahren können, sind enorm vom ÖPNV abhängig.
  <br>

  Bei diesem Zustand: Kein Wunder das immernoch die meisten Menschen, die sich ein Auto leisten können, auch mit diesem in Bayreuth fahren. Wäre der ÖPNV kostenlos und deutlich besser ausgebaut würden auch deutlich mehr Menschen aufs Auto verzichten, vielen anderen Menschen wäre die Teilhabe am Leben der Stadt ermöglicht und die Vernetzung mit dem Landkreis verbessert.	</details>
<br>

- Klimanotstand ausrufen
  <details><summary>
  Das Aufhalten der Klimakrise muss aktuell absolute Prioriät sein. Die Stadt würde mit dem ausrufen des Klimanotstandes die existenzielle Bedrohung durch die Klimakrise anerkennen.
  </summary>
  <br>

  Die Klimakrise ist eine Krise dessen Ausmaß, wir nur noch innerhalb der nächsten Jahre, massiv einschränken können. Tun wir dies nicht, werden die Auswirkungen noch katastrophaler: Stürme, der Anstieg des Meeresspiegels, Hungersnöte, Dürren, Starkregen und weitere Extremwetterereignisse werden ganze Städte vernichten und unzähligen Menschen ihre Lebensgrundlage oder sogar ihr Leben kosten. Durch die Erreichung von Kipppunkten im Klimasystem ist das ganze auch noch unumkehrbar. Das alles ist keine Zukunftsmusik. Schon jetzt trifft es viele Gebiete und auch hier sind die Folgen der menschengemachten Klimakrise spürbar. In Bayreuth wird die Erwärmung vorraussichtlich, verglichen zum weltweiten Durchschnitt, doppelt so stark sein und die Gefahr von Überschwemmungen und Dürren steigt an. [6]
  <br>
  
  Die Einschränkund dieser Krise muss auch für die Stadt Bayreuth Priorität haben. Mit dem Ausrufen des Klimanotstands würde die Stadt, so wie viele andere Städte und Kommunen zuvor, die existenzielle Bedrohung der Klimakrise anerkennen. Alle Entscheidungen der Stadt würden dann unter dem Aspekt der Klimakrise betrachtet, so wie bisher alles unter dem Aspekt der Wirtschaftlichkeit betrachtet wird.</details>
<br>

<details><summary>
Quellen
</summary>

- [1] Potentialanalysen für das Klimaschutzkonzept der Stadt Bayreuth; Zwischenbericht vom 28.09.2021; Energievision Franken

- [2] Wuppertal Institut (2020). CO2-neutral bis 2035: Eckpunkte eines deutschen Beitrags zur Einhaltung der 1,5-°C-Grenze. Bericht. Wuppertal.

- [3] Klimapakt der Europäischen Metropolregion Nürnberg; beschlossen in der Ratsversammlung am 28.07.2017, https://klimaschutz.metropolregionnuernberg.de

- [4] Erläuterungen und Infos zum Mieter:innenstrom: ![https://www.bundesnetzagentur.de/DE/Vportal/Energie/Vertragsarten/Mieterstrom/start.html](https://www.bundesnetzagentur.de/DE/Vportal/Energie/Vertragsarten/Mieterstrom/start.html)

- [5] Forderungen des Radentscheid Bayreuth (2020): ![https://radentscheid-bayreuth.de/forderungen/](https://radentscheid-bayreuth.de/forderungen/)

- [6] Vortrag StadtKlimaWandel in Bayreuth vom 26.06.2019 ![https://www.bayreuth.de/wp-content/uploads/2019/07/Vortrag_SR_2019-06-26_StadtklimaWandel_ProfThomas_gesch.pdf](https://www.bayreuth.de/wp-content/uploads/2019/07/Vortrag_SR_2019-06-26_StadtklimaWandel_ProfThomas_gesch.pdf)</details>
