---
layout: page
title: Kontakt
permalink: /kontakt/
nav_order: 6
---

Du hast Fragen zu unserer Aktion oder möchtest uns kennenlernen? Dann schreib uns gerne!

Email: 	Klimacamp_bayreuth@riseup.net

Instagram: [KlimabaumhausBayreuth](https://www.instagram.com/klimabaumhausbayreuth)

Facebook: [KlimabaumhausBayreuth](https://www.facebook.com/KlimabaumhausBayreuth-102467872275481)

Twitter: [KlimabaumhausBayreuth](https://twitter.com/KlimabaumhausBt)

Telegram: [KlimabaumhausBayreuth](https://t.me/Klimabaumhausbayreuth)
