---
layout: page
title: Klimabaumhaus Bayreuth
permalink: /
nav_order: 1
---

<!--REDIRECT_NEW-->
  <script>window.location.href = "https://www.klimacamp-bayreuth.de/";</script>
<!--REDIRECT_NEW-->

![plogo](/assets/logo.jpg){: width="350" }

Wir sind eine Gruppe Klimagerechtigkeitsaktivist*innen und haben von November 2021 bis Januar 2022 ein Protestbaumhaus in Bayreuth gebaut.

Im Angesicht der Klimakatastrophe fordern wir die Stadt Bayreuth auf Verantwortung zu übernehmen und als ersten Schritt dafür unseren Forderungen nachzukommen.

Während die Effekte der Klimakrise immer stärker werden, verschiebt die Politik gedankenlos ihre Verantwortung auf die nächste Generationen. Die Klimakatastrophe könnte jetzt noch massiv eingedämmt werden, indem konkrete klimaschützende Maßnahmen SOFORT beschlossen werden. Es ist noch nicht zu spät aber wir haben keine Zeit mehr um weiter zu diskutieren. Es ist klar wo das Problem liegt und es ist auch klar mit welchen Maßnahmen wir die Klimakrise mildern können. Heute ist die Zeit zum Handeln, denn die Alternative ist eine humanitäre Katastrophe!

Wir sind eine Gruppe von Klimagerechtigkeitsaktivist*innen die sich gegen diese Katastrophe einsetzen, deshalb haben wir Forderungen an die Stadt formuliert und werden so lange aktiv bleiben, bis diese auch umgesetzt werden! Unter dem Motto "Das Baumhaus geht, der Protest geht weiter!" werden wir auch in Zukunft kreative Aktionen zu unseren Forderungen an die Stadt organisieren. 

**Corona-Regeln**

Auf unseren Veranstaltungen gelten selbstverständlich weiterhin die gesetzlich vorgegebenen Corona-Schutzmaßnahmen als Mindestanforderungen. Wir möchten weiterhin alle Menschen dazu auffordern sich regelmäßig testen zu lassen, Abstände einzuhalten und Maske zu tragen. Wir sprechen uns entschieden gegen wissenschaftsfeindliche Aussagen sowohl in Bezug auf die Klimakrise als auch in Bezug auf die Corona-Pandemie aus.
